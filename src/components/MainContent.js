import React from 'react'
import Features from './Features'
import Footer from './Footer'
import GetSwiggy from './GetSwiggy'
import TopSection from './TopSection'

function MainContent() {
  return (
    <div>
        <TopSection></TopSection>
        <Features></Features>
        <GetSwiggy></GetSwiggy>
        <Footer></Footer>
    </div>
  )
}

export default MainContent