import React from 'react'
import playstore from '../components/images/playstore.png'
import itunes from '../components/images/itunes.png'
import iphone from '../components/images/iphone.png'
import pixel from '../components/images/pixel.png'

function GetSwiggy() {
    return (
        <div>
            <div className='container'>
                <div className='row d-flex flex-row justify-content-around'>
                    <div className='col-12 col-lg-4 d-flex flex-column justify-content-center align-items-center mx-5 px-5'>
                        <h1 className='text-start swiggy-heading'>Restaurants in your pocket</h1>
                        <p className='text-start swiggy-para'>
                            Order from your favorite restaurants & track on the go, with the all-new Swiggy app.
                        </p>
                        <div className='d-flex justify-content-center'>
                            <img src={playstore} alt='play-store' className='w-50 m-2'></img>
                            <img src={itunes} alt='itunes' className='w-50 m-2'></img>
                        </div>
                    </div>
                    <div className='col-12 col-lg-6 d-flex flex-row justify-content-center mt-3 mt-lg-0'>
                        <img src={pixel} alt='pixel' className='w-50'></img>
                        <img src={iphone} alt='iphone' className='w-50 mt-5'></img>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default GetSwiggy