import React, { Component } from 'react'
import '../index.css'
import validator from 'validator'

class Signup extends Component {

    constructor(props) {
        super(props)

        this.state = {
            name: '',
            phone: '',
            email: '',
            floatingPassword: '',
            nameError: '',
            phoneError: '',
            emailError: '',
            floatingPasswordError: '',
            success: '',
            final: false,
        }
    }

    // dashboard = useNavigate()

    handleChange = (event) => {
        const elementName = event.target.id;
        this.setState({
            [elementName]: event.target.value,
        });
    }

    handleSubmit = () => {
        let flag = true

        if (!(validator.isMobilePhone(this.state.phone, ['en-IN']))) {
            flag = false
            this.setState({
                phoneError: 'Please enter a valid phone number!',
            })
        } else {
            flag = true
            this.setState({
                phoneError: ''
            })
        }

        if (!validator.isAlpha(this.state.name)) {
            flag = false
            this.setState({
                nameError: 'Please enter a valid name!',
            })
        } else {
            flag = true
            this.setState({
                nameError: '',
            })
        }

        if (!validator.isEmail(this.state.email)) {
            flag = false
            this.setState({
                emailError: 'Please enter a valid email!',
            })
        } else {
            flag = true
            this.setState({
                emailError: ''
            })
        }

        if (this.state.floatingPassword.length < 6) {
            flag = false
            this.setState({
                floatingPasswordError: 'Please enter minimum 6 characters',
            })
        } else {
            flag = true
            this.setState({
                floatingPasswordError: '',
            })
        }

        if (flag === true) {
            this.setState({
                name: '',
                phone: '',
                email: '',
                floatingPassword: '',
                success: 'Congratulations! You\'ve succesfully signed up! ',
                final: true,
            })
        }

    }

    render() {
        return (
            <div>
                <div className='container mt-5'>
                    <div className='row d-flex justify-content-center'>
                        <div className='d-flex justify-content-around align-items-center col-10 col-md-7'>
                            <h3>Sign Up</h3>
                            <img
                                src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto/Image-login_btpq7r'
                                alt='shawarma'
                                className='shawarma'>
                            </img>
                        </div>
                        <div className='col-10 col-md-6'>
                            <div className="form-floating mt-3">
                                <input type="text" value={this.state.phone} onChange={this.handleChange} className="form-control" id="phone" placeholder="name@example.com" />
                                <label htmlFor="phone">Phone Number</label>
                            </div>
                            <span className='my-1'>{this.state.phoneError.length > 0 ? <div className='text-danger'>{this.state.phoneError}</div> : <div> &nbsp; </div>}</span>
                            <div className="form-floating">
                                <input value={this.state.name} type="text" onChange={this.handleChange} className="form-control" id="name" placeholder="name@example.com" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <span className='my-1'>{this.state.nameError.length > 0 ? <div className='text-danger'>{this.state.nameError}</div> : <div> &nbsp; </div>}</span>
                            <div className="form-floating">
                                <input value={this.state.email} type="email" onChange={this.handleChange} className="form-control" id="email" placeholder="name@example.com" />
                                <label htmlFor="email">Email</label>
                            </div>
                            <span className='my-1'>{this.state.emailError.length > 0 ? <div className='text-danger'>{this.state.emailError}</div> : <div> &nbsp; </div>}</span>
                            <div className="form-floating">
                                <input value={this.state.floatingPassword} type="password" onChange={this.handleChange} className="form-control" id="floatingPassword" placeholder="Password" />
                                <label htmlFor="floatingPassword">Password</label>
                            </div>
                            <span className='my-1'>{this.state.floatingPasswordError.length > 0 ? <div className='text-danger'>{this.state.floatingPasswordError}</div> : <div> &nbsp; </div>}</span>
                            <button onClick={this.handleSubmit} className="btn btn-warning mt-3 w-100" type="button">CONTINUE</button>
                            <p className='mt-2 span-dark text-start'>By creating an account, I accept the Terms & Conditions & Privacy Policy</p>
                            <div className='mt-3 text-success'>{this.state.success.length > 0 ? <div><h4>{this.state.success}</h4> </div> : <h2>&nbsp;</h2>}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Signup