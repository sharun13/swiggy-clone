import React from 'react'
import logo from '../components/images/swiggy-svg.svg'
import social from '../components/images/social.png'
import itunes from '../components/images/itunes.png'
import play from '../components/images/playstore.png'

function Footer() {
    return (
        <div className='footer'>
            <div className='container d-flex justify-content-center p-5'>
                <div className='row'>
                    <div className='col-12 col-md-3 text-start text-wrap'>
                        <ul>
                            <p className='footer-para'>COMPANY</p>
                            <li>About Us</li>
                            <li>Team</li>
                            <li>Careers</li>
                            <li>Swiggy Blog</li>
                            <li>Bug Bounty</li>
                            <li>Swiggy One</li>
                            <li>Swiggy Corporate</li>
                            <li>Swiggy Installment</li>
                        </ul>
                    </div>
                    <div className='col-12 col-md-3 text-start text-wrap'>
                        <ul>
                            <p className='footer-para'>CONTACT</p>
                            <li>Help & Support</li>
                            <li>Partner with us</li>
                            <li>Ride with us</li>
                        </ul>
                    </div>
                    <div className='col-12 col-md-3 text-start text-wrap'>
                        <ul>
                            <p className='footer-para'>LEGAL</p>
                            <li>Terms & Conditions</li>
                            <li>Refund & Cancellation</li>
                            <li>Privacy Policy</li>
                            <li>Cookie Policy</li>
                            <li>Offer Terms</li>
                            <li>Phishing & Fraud</li>
                            <li>Corporate – Swiggy Money Codes Terms and Conditions</li>
                            <li>Corporate - Swiggy Discount Voucher Terms and Conditions</li>
                        </ul>
                    </div>
                    <div className='col-12 col-md-3 d-flex flex-column align-items-end'>
                        <img src={itunes} alt='itunes' className='w-50 m-3'></img>
                        <img src={play} alt='play-store' className='w-50 m-3'></img>
                    </div>
                </div>
            </div>
            <hr></hr>
            <div className='container py-2'>
                <div className='row d-flex justify-content-around align-items-center'>
                    <div className='col-12 col-md-4'>
                        <img src={logo} alt='logo' className='w-50'></img>
                    </div>
                    <div className='col-12 col-md-4 mt-3 mt-md-0'>
                        <h5> © 2022 Swiggy </h5>
                    </div>
                    <div className='col-12 col-md-4'>
                        <img src={social} alt='social' className='w-50'></img>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer