import React from 'react'
import logo from './images/Swiggy_logo.svg.png'
import '../index.css'
import { useNavigate } from 'react-router-dom'

function TopSection() {
    const navigate = useNavigate()
    return (
        <div>
            <div className='d-flex flex-row'>
                <div className='container-fluid w-75 mt-5'>
                    <div className='row'>
                        <div className='col-12 d-flex flex-row justify-content-around align-items-center'>
                            <img src={logo} alt='logo' className='w-25' />
                            <div className='d-flex flex-row justify-content-end align-items-center'>
                                {/* <h6 className='fw-bold'>Login</h6> */}
                                <button
                                    className='btn btn-info'
                                    onClick={() => navigate('dashboard')}
                                >View Restaurants</button>
                                <button className='btn btn-dark m-4'
                                    onClick={() => navigate('sign-up')}>
                                    Sign Up
                                </button>
                            </div>
                        </div>
                        <div className='col-12 d-flex justify-content-center'>
                            <div className='d-flex flex-column align-items-start gap-2 w-75'>
                                <h1 className='fw-bold mt-5 main-heading'>Cooking gone wrong?</h1>
                                <h4 className='order-food'>Order food from favourite restaurants near you</h4>
                                <div className="input-group mt-3 mb-3">
                                    <input type="text" className="form-control" placeholder="Enter your delivery location" aria-label="Recipient's username" aria-describedby="button-addon2" />
                                    <button className="btn btn-warning fw-bolder" type="button" id="button-addon2">FIND FOOD</button>
                                </div>
                                <p className='popular'>POPULAR CITIES IN INDIA</p>
                                <p className='text-start'>
                                    <span className='span-dark'>Ahmedabad </span>
                                    <span className='span-light'>Bangalore </span>
                                    <span className='span-dark'>Chennai </span>
                                    <span className='span-light'>Delhi </span>
                                    <span className='span-dark'>Gurgaon </span>
                                    <span className='span-light'>Hyderabad </span>
                                    <span className='span-dark'>Kolkata </span>
                                    <span className='span-light'>Mumbai </span>
                                    <span className='span-dark'>Pune </span>
                                    <span className='span-light'>& more.</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='w-50 d-none d-xl-block'>
                    <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,h_1340/Lunch1_vlksgq' alt='lunch1' className='w-100' />
                </div>
            </div>
        </div>
    )
}

export default TopSection