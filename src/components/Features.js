import React from 'react'

function Features() {
    return (
        <div>
            <div className='container-fluid features p-5 d-flex flex-row justify-content-around'>
                <div className='row'>
                    <div className='d-flex flex-column justify-content-center align-items-center p-2 col-12 col-md-4'>
                        <img className='w-25 mb-4' src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_210,h_398/4x_-_No_min_order_x0bxuf' alt='minimum-order' ></img>
                        <h3 className='m-2 feature-heading'>No Minimum Order</h3>
                        <p className='feature-para px-5'>Order in for yourself or for the group, with no restrictions on order value</p>
                    </div>
                    <div className='d-flex flex-column justify-content-center align-items-center p-2 col-12 col-md-4'>
                        <img className='w-25 mb-4' src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_224,h_412/4x_Live_order_zzotwy' alt='live-tracking' ></img>
                        <h3 className='m-2 feature-heading'>Live Order Tracking</h3>
                        <p className='feature-para px-5'>Know where your order is at all times, from the restaurant to your doorstep</p>
                    </div>
                    <div className='d-flex flex-column justify-content-end align-items-center p-2 col-12 col-md-4'>
                        <img className='w-25 mb-4' src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_248,h_376/4x_-_Super_fast_delivery_awv7sn' alt='quick-delivery' ></img>
                        <h3 className='m-2 feature-heading'>Lightning-Fast Delivery</h3>
                        <p className='feature-para px-5'>Experience Swiggy's superfast delivery for food delivered fresh & on time</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Features