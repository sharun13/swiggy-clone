import './App.css';
import MainContent from './components/MainContent';
import { Routes, Route } from 'react-router-dom';
import Signup from './components/Signup';
import Dashboard from './components/Dashboard';

function App() {
  
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<MainContent></MainContent>}></Route>
        <Route path='sign-up' element={<Signup></Signup>}></Route>
        <Route path='dashboard' element={<Dashboard></Dashboard>}></Route>
      </Routes>
    </div>
  );
}

export default App;
